
import FlyModules from 'zunzun/flymodule/index.mjs'

export default class {
  constructor(cfg, srv_mg) {
    this.srv_mg = srv_mg;
    this.name = cfg.name;
    this.path = cfg.path;
    this.config = cfg.config;
  }

  async start() {
    try {
      this.instance = await FlyModules.start_service("twigs/srv/"+this.name, this.path, this.config, this.srv_mg);
    } catch (e) {
      console.error(e.stack);
    }
  }
}
