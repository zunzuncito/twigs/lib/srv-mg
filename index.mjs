
import fs from 'fs'
import path from 'path'

import Service from './service.mjs'

const SRV_PATH = "twigs/srv"

export default class ServiceManager {
  constructor(cfg_layers, app_cfg) {
    this.app_cfg = app_cfg;
    this.path = app_cfg.path;

    const zunzun_twigs_srv_path = path.resolve(app_cfg.path, SRV_PATH);

    this.service_layers = [];
    this.running_services = [];

    for (let cfg_layer of cfg_layers) {
      let srv_layer = [];
      for (let srv_cfg of cfg_layer) {
        srv_layer.push(new Service(srv_cfg, this));
      }
      this.service_layers.push(srv_layer);
    }
  }

  async preconf() {
    try {
      for (let qgroup of this.service_layers) {
        for (let qsrv of qgroup) {
          let zz_over_config = {};
          for (let zzcfg_srv of this.app_cfg.services) {
            if (zzcfg_srv.name === qsrv.name) {
              zz_over_config = zzcfg_srv.config;
              if (zzcfg_srv.disabled) qsrv.disabled = true;
            }
          }
          qsrv.config = zz_over_config
        }
      }
    } catch (e) {
      console.error(e.stack);
      process.exit();
    }
  }

  async start() {
    try {
      while (this.service_layers.length > 0) {
        let qgroup = this.service_layers[0];

        for (let qsrv of qgroup) {
          if (!qsrv.disabled) {
            await qsrv.start();
            this.running_services.push(qsrv);
          } else {
            console.log(`Service '${qsrv.name}' disabled!`);
          }
        }

        this.service_layers.splice(0, 1)
      }
    } catch (e) {
      console.error(e.stack);
      process.exit();
    }
  }

  get_service(name) {
    for (let qgroup of this.service_layers) {
      for (let qsrv of qgroup) {
        if (name === qsrv.name) {
          return qsrv;
        }
      }
    }
    for (let srv of this.running_services) {
      if (name === srv.name) {
        return srv;
      }
    }
    throw new Error(`

          Error: no service named ${name} was found

    `);
  }

  get_service_instance(name) {
    return this.get_service(name).instance.instance_object;
  }
}
